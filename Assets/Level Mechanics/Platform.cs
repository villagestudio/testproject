﻿using UnityEngine;
using System.Collections;

public class Platform : MonoBehaviour {

    public float left = -10;
    public float right = 10;

    public float speed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (transform.position.x >= right || transform.position.x <= left)
            speed = -speed;

        Vector2 pos = transform.position;
        pos.x += speed;
        transform.position = pos;

	}
}
