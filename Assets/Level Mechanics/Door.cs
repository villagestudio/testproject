﻿using UnityEngine;
using System.Collections;

public class Door : MonoBehaviour {

    public Button myButton;

	// Use this for initialization
	void Start () {
	
	}

    public void Open()
    {
        GetComponent<SpriteRenderer>().enabled = false;
        GetComponent<BoxCollider2D>().enabled = false;
    }
	
    public void Close()
    {
        GetComponent<SpriteRenderer>().enabled = true;
        GetComponent<BoxCollider2D>().enabled = true;
    }

	// Update is called once per frame
	void Update () {
	    if(myButton.IsActivated())
        {
            this.Open();
        }

        else
        {
            this.Close();
        }
	}
}
