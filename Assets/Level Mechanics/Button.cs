﻿using UnityEngine;
using System.Collections;

public class Button : MonoBehaviour {

    
	// Use this for initialization
    bool Activated = false;

    public Sprite Off;
    public Sprite On;

    public bool IsActivated()
    {
        return Activated;
    }
    public void toggle()
    {
        if(Activated)
        {
            Activated = false;
            this.GetComponent<SpriteRenderer>().sprite = Off;
        }
        else
        {
            Activated = true;
            this.GetComponent<SpriteRenderer>().sprite = On;
        }
    }

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	    
	}
}
