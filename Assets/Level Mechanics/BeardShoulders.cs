﻿using UnityEngine;
using System.Collections;

public class BeardShoulders : MonoBehaviour {

    float offset = 0;
    float sign = 1;
    public float sup = 10;
    public float inf = 10;
    public float Add = 25;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        if (offset >= sup)
            sign = -1.5f;

        if (offset <= inf)
            sign = 1;

        offset += sign * Time.deltaTime;
        Vector2 pos = transform.position;
        pos.y += sign * Time.deltaTime;
        transform.position = pos;
	}

    void OnTriggerEnter2D(Collider2D x)
    {
        if (x.tag == "Player")
        {
            x.GetComponent<PlayerLife>().mana.value += Add;
            Destroy(gameObject);
        }
    }
}
