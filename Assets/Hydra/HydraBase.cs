﻿using UnityEngine;
using System.Runtime.InteropServices;
using System.Collections;

public class HydraBase : MonoBehaviour
{

    public static class HydraButtonState
    {
        public const int Bumper = 128;
        public const int Joystick = 256;
        public const int B1 = 32;
        public const int B2 = 64;
        public const int B3 = 8;
        public const int B4 = 16;
        public const int Start = 1;
    }

    /// <summary>
    /// Is true if the Sixense library has been initialised. 
    /// By default it will auto init on the first Hydra. call.
    /// </summary>
    public static bool Initialized = false;


    /// <summary>
    /// This data type holds data for a single hydra controller,
    /// and can be used with the GetNewestData function.
    /// </summary>
    public struct ControllerData
    {
        public Vector3 Position;
        public Vector3 RotMatX;
        public Vector3 RotMatY;
        public Vector3 RotMatZ;
        public float JoystickX;
        public float JoystickY;
        public float Trigger;
        public int Buttons;
        public byte SequenceNumber;
        public Quaternion Rotation;
        public short FirmwareRevision;
        public short HardwareRevision;
        public short PacketType;
        public short MagneticFrequency;
        public int Enabled;
        public int ControllerIndex;
        public byte IsDocked;
        public byte WhichHand;
        public byte HemiTrackingEnabled;
    };

    /// <summary>
    /// This data type holds data for up to 4 controllers and is used with the GetAllData function.
    /// </summary>
    public struct AllControllerData
    {
        public ControllerData[] ControllerNumber;
    }

    #region P/invoke
    [DllImport("sixense")]
    private static extern int sixenseInit();

    [DllImport("sixense")]
    private static extern int sixenseExit();

    [DllImport("sixense")]
    private static extern int sixenseGetNewestData(int which, out ControllerData data);

    [DllImport("sixense")]
    private static extern int sixenseSetFilterEnabled(int filterEnabled);

    [DllImport("sixense")]
    private static extern int sixenseIsBaseConnected(int base_num);

    [DllImport("sixense")]
    private static extern int sixenseIsControllerEnabled(int which);

    [DllImport("sixense")]
    private static extern int sixenseGetMaxBases();

    [DllImport("sixense")]
    private static extern int sixenseSetActiveBase(int baseNum);

    [DllImport("sixense")]
    private static extern int sixenseGetMaxControllers();

    [DllImport("sixense")]
    private static extern int sixenseGetNumActiveControllers();

    [DllImport("sixense")]
    private static extern int sixenseGetAllNewestData(out AllControllerData allData);

    [DllImport("sixense")]
    private static extern int sixenseGetAllData(int indexBack, out AllControllerData allData);

    [DllImport("sixense")]
    private static extern int sixenseGetData(int which, int indexData, out ControllerData data);

    [DllImport("sixense")]
    private static extern int sixenseGetHistorySize();

    [DllImport("sixense")]
    private static extern int sixenseGetFilterEnabled(out int filterEnabled);

    [DllImport("sixense")]
    private static extern int sixenseSetFilterParams(float nearRange, float nearVal, float farRange, float farVal);

    [DllImport("sixense")]
    private static extern int sixenseGetFilterParams(out float nearRange, out float nearVal, out float farRange, out float farVal);

    [DllImport("sixense")]
    private static extern int sixenseAutoEnableHemisphereTracking(int whichController);

    [DllImport("sixense")]
    private static extern int sixenseSetHighPriorityBindingEnabled(int onOrOff);

    [DllImport("sixense")]
    private static extern int sixenseGetHighPriorityBindingEnabled(out int onOrOff);
    #endregion



    /// <summary>
    /// This function initializes the Sixense library. 
    /// It must be called at least one time per application. Subsequent calls will have no effect. 
    /// Once initialized, the other Sixense function calls will work as described until Finish() is called. 
    /// </summary>
    /// <returns>True is returned if the library is successfully initialized; otherwise, the return value is false</returns>
    public static bool Init()
    {
        if (!Initialized)
        {
            Initialized = true;
            return (sixenseInit() == 0);
        }
        return false;
    }
    /// <summary>
    /// This shuts down the Sixense library. After this function call, all Sixense API calls will return failure until Init() is called again.
    /// </summary>
    /// <returns>True is returned if the library was successfully shut down; otherwise, the return value is False.  </returns>
    public static bool Finish()
    {
        Initialized = false;
        return (sixenseExit() == 0);
    }
    /// <summary>
    /// This call can be used to enable or disable filtering of the controller data. 
    /// The filter parameters are not affected by this call.
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="enabled">The desired state of the filtering</param>
    public static void SetFilterEnabled(bool enabled)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseSetFilterEnabled(enabled ? 1 : 0);
    }
    /// <summary>
    ///  This function returns the most recent state of one of the connected Sixense controllers. 
    ///  If the controller is not connected it will print that out in the output window but not stop the program.
    ///  Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="id">The ID of the desired controller. Valid values are from 0 to 3. 
    /// If the desired controller is not connected, an empty data packet is returned. 
    /// Empty data packets are initialized to a zero position and the identity rotation matrix.</param>
    /// <param name="mydata">A pointer to user-allocated memory for returning the desired controller information. 
    /// The data type is Hydra.ControllerData</param>
    public static void GetNewestData(int id, out ControllerData mydata)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        if (sixenseGetNewestData(id, out mydata) == -1) Debug.Log("Controller " + id + " is not connected!");
    }
    /// <summary>
    /// This call is used to determine whether or not a given controller is powered on and connected to the system.
    /// </summary>
    /// <param name="which">The argument is an index between 0 and the maximum number of supported controllers.</param>
    /// <returns>This call returns the connection status of the referenced controller. 
    /// True means the controller is enabled, false means it is disabled. </returns>
    public static bool IsControllerEnabled(int which)
    {
        return (sixenseIsControllerEnabled(which) == 1);
    }
    /// <summary>
    /// This call returns whether or not the designated base unit is attached to the system. 
    /// Calling IsBaseConnected() can be used by the game to enable or disable Sixense support.  
    /// </summary>
    /// <returns>This call returns true if the base is currently plugged in and false otherwise. </returns>
    public static bool IsBaseConnected()
    {
        return (sixenseIsBaseConnected(0) == 1);
    }
    /// <summary>
    /// At the current time the Sixense driver supports a maximum of 4 simultaneous base units. 
    /// Since this number may change in the future, GetMaxBases() should be called when iterating through all bases to ensure compatibility. 
    /// Note that the bases have to have different magnetic frequencies in order to not interfere with each other, and current retail products like the Razer Hydra only support one frequency. 
    /// </summary>
    /// <returns> This call returns the maximum number of base units supported by the Sixense control system. Currently, this number is 4 for all platforms.></returns>
    public static int GetMaxBases()
    {
        return sixenseGetMaxBases();
    }
    /// <summary>
    /// It is possible for the Sixense API to address multiple bases connected to the same computer. 
    /// This call can be used to designate which base all subsequent API calls are directed towards
    /// </summary>
    /// <param name="baseNum">An integer from 0 to GetMaxBases()-1 </param>
    /// <returns> True is returned if the the designated base is active and valid; otherwise, the return value is false</returns>
    public static bool SetActiveBase(int baseNum)
    {
        return (sixenseSetActiveBase(baseNum) == 0);
    }
    /// <summary>
    /// Gives the maximum number of controllers.
    /// </summary>
    /// <returns>This call returns the maximum number of controllers supported by the Sixense control system. Currently, this number is 4 for all platforms.</returns>
    public static int GetMaxControllers()
    {
        return sixenseGetMaxControllers();
    }
    /// <summary>
    /// This call returns the maximum number of controllers supported by the Sixense control system. Currently, this number is 4 for all platforms. 
    /// </summary>
    /// <returns> An int representing the number of controllers supported. </returns>
    public static int GetNumActiveControllers()
    {
        return sixenseGetNumActiveControllers();
    }
    /// <summary>
    /// This function gives the most recent state of all of the Sixense controllers. 
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="data">A pointer to user-allocated memory for returning the controller information.</param>
    public static void GetAllNewestData(out AllControllerData data)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseGetAllNewestData(out data);
    }
    /// <summary>
    /// This function returns the most recent state of all of the Sixense controllers, looking back in history if desired. 
    /// When used in conjunction with the sequence numbers in the data packets, 
    /// this function is useful for referencing packets that may have been skipped due to the game’s frame rate relative to the 60Hz update rate of the controller. 
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="indexBack"> How far back in the history buffer to retrieve data. 0 returns the most recent data, 9 returns the oldest data. Any of the last 10 positions may be queried. </param>
    /// <param name="allData"> A pointer to user-allocated memory for returning the desired controller information. </param>
    public static void GetAllData(int indexBack, out AllControllerData allData)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseGetAllData(indexBack, out allData);
    }
    /// <summary>
    /// This function returns the current state of one of the connected Sixense controllers, 
    /// looking back in history if desired. When used in conjunction with the sequence numbers in the data packets, 
    /// this function is useful for referencing packets that may have been skipped due to the games frame rate relative to the 60Hz update rate of the controller.
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="which"> The ID of the desired controller. </param>
    /// <param name="indexData"> How far back in the history buffer to retrieve data. 0 returns the most recent data, 9 returns the oldest data.</param>
    /// <param name="data">A pointer to user-allocated memory for returning the desired controller information. </param>
    public static void GetData(int which, int indexData, out ControllerData data)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        if (sixenseGetData(which, indexData, out data) == -1) Debug.Log("Controller " + which + " is not connected!"); ;
    }
    /// <summary>
    ///  This can be used when the application is checking data less frequently than the data is arriving from the USB port.
    /// </summary>
    /// <returns> The size of the history buffer is returned. </returns>
    public static int GetHistorySize()
    {
        return sixenseGetHistorySize();
    }
    /// <summary>
    /// This call is used to determine whether or not the controllers are filtering their positions and orientations. 
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <returns>Return true if the filter is enabled and false if it is not.</returns>
    public static bool GetFilterEnabled()
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        int filterEnabled;
        sixenseGetFilterEnabled(out filterEnabled);
        return (filterEnabled == 1);
    }
    /// <summary>
    /// This function sets the filter parameters. The Sixense controllers have built-in filtering capabilities. The filter is an Exponentially Weighted Moving Average Filter.
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="nearRange">The range from the Sixense Base Unit at which to start increasing the filtering level from the near_val to far_val.
    /// Between near_range and far_range, the near_val and far_val are linearly interpolated</param>
    /// <param name="nearVal">The minimum filtering value. This value is used for when the controller is between 0 and near_val millimeters from the Sixense Base Unit.
    /// Valid values are between 0 and 1. </param>
    /// <param name="farRange"> The range from the Sixense Base Unit after which to stop interpolating the filter value from the near_val, and after which to simply use far_val. </param>
    /// <param name="farVal">which to simply use far_val. far_val  The maximum filtering value. This value is used for when the controller is between far_val and infinity. Valid values are between 0 and 1. </param>
    public static void SetFilterParams(float nearRange, float nearVal, float farRange, float farVal)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseSetFilterParams(nearRange, nearVal, farRange, farVal);
    }
    /// <summary>
    /// Set the parameters that control the position and orientation filtering level. 
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="nearRange">The range from the Sixense Base Unit at which to start increasing the filtering level from the near_val to far_val.
    /// Between near_range and far_range, the near_val and far_val are linearly interpolated</param>
    /// <param name="nearVal">The minimum filtering value. This value is used for when the controller is between 0 and near_val millimeters from the Sixense Base Unit.
    /// Valid values are between 0 and 1. </param>
    /// <param name="farRange"> The range from the Sixense Base Unit after which to stop interpolating the filter value from the near_val, and after which to simply use far_val. </param>
    /// <param name="farVal">which to simply use far_val. far_val  The maximum filtering value. This value is used for when the controller is between far_val and infinity. Valid values are between 0 and 1. </param>
    public static void GetFilterParams(out float nearRange, out float nearVal, out float farRange, out float farVal)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        GetFilterParams(out nearRange, out nearVal, out farRange, out farVal);
    }
    /// <summary>
    /// Enable Hemisphere Tracking when the controller is aiming at the base. This call is deprecieated, as hemisphere tracking is automatically enabled when the controllers are in the dock or by the sixenseUtils::controller_manager.
    /// Note if Init has not been called this function will throw an exception.
    /// </summary>
    /// <param name="whichcontroller"> The controller id </param>
    public static void AutoEnableHemisphereTracking(int whichcontroller)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseAutoEnableHemisphereTracking(whichcontroller);
    }
    /// <summary>
    /// This function enables and disables High Priority RF binding mode. This call is only used with the wireless Sixense devkits. 
    /// </summary>
    /// <param name="onOrOff">True enables High Priority binding, false disables it.</param>
    public static void SetHighPriorityBindingEnabled(bool onOrOff)
    {
        if (!Initialized) throw new System.InvalidOperationException("The Sixense library was not initialized!");
        sixenseSetHighPriorityBindingEnabled(onOrOff ? 1 : 0);
    }

    // Use this for initialization
    void Start()
    {
        Init();
        SetFilterEnabled(true);
    }

    // Update is called once per frame
    void Update()
    {

    }
}
