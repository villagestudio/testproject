﻿using UnityEngine;
using System.Collections;

public class PointCollide : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        this.transform.rotation = Quaternion.Euler(Vector3.zero);
	}

    void OnTriggerEnter2D(Collider2D x)
    {
        try
        {
            if (x != null && x.transform.parent != null && x.transform.parent.tag == "Monster")
            {
                EnemyController ec = x.GetComponentInParent<EnemyController>();
                ec.TakeDamage(5, transform.position, 5, false);
            }
        }
        catch(UnassignedReferenceException ex)
        {

        }

        if (x.gameObject.GetComponent<Button>() != null)
            x.gameObject.GetComponent<Button>().toggle();
    }

}
