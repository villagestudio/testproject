﻿using UnityEngine;
using System.Collections;

public class HydraCursor : MonoBehaviour
{

    HydraBase.ControllerData controllerData;
    public float Dampening = 20;
    public int Which = 0;
    public GameObject Player;
    public bool Enabled = false;


    // Use this for initialization
    void Start()
    {
        Debug.Log("HydraBase Initialize: " + HydraBase.Init());
        HydraBase.SetFilterEnabled(true);
        controllerData = new HydraBase.ControllerData();

        HydraBase.GetNewestData(Which, out controllerData);
    }

    // Update is called once per frame
    void Update()
    {
        if (HydraBase.IsBaseConnected())
        {
            HydraBase.GetNewestData(Which, out controllerData);
            Vector3 newRot = controllerData.Rotation * Vector3.forward;
            if (controllerData.IsDocked == 0)
            {
                Vector3 pos = this.transform.position;

                pos.x = Player.transform.position.x - newRot.x * Dampening;
                pos.y = Player.transform.position.y - newRot.y * Dampening;

                this.transform.position = pos;

            }
        }
    }

    void OnCollisionEnter(Collision x) {
        if(Enabled) // not walking
        {
            //Deal damage
        }
    }
}
