﻿using UnityEngine;
using System.Collections;

public class MustacheController : MonoBehaviour
{

    public HydraCursor cursor;
    public Vector2 local_pos;
    public float angle;
    Mesh mymesh;
    int end;

    // Use this for initialization
    void Start()
    {
        mymesh = this.GetComponent<MeshFilter>().mesh;
        Debug.Log("Vertex count= " + mymesh.vertices.Length);

        float ylocal = mymesh.vertices[0].y;
        for (int i = 0; i < mymesh.vertices.Length; i++)
        {
            if (mymesh.vertices[i].y < ylocal)
            {
                ylocal = mymesh.vertices[i].y;
                end = i;
            }
        }


    }

    // Update is called once per frame
    void Update()
    {
        /*
        Vector3[] local_vertices = new Vector3[mymesh.vertices.Length];
        local_vertices = mymesh.vertices;
        for (int i = 0; i < mymesh.vertices.Length; i++)
        {
            //local_vertices[i] += new Vector3(0, 0.05f, 0) * Time.deltaTime * i;
        }
        mymesh.vertices = local_vertices;*/

        local_pos = this.transform.parent.transform.position;
        Vector2 hydra_pos = cursor.transform.position;
        hydra_pos -= local_pos;
        hydra_pos.Normalize();

        angle = Mathf.Rad2Deg * Mathf.Atan2(hydra_pos.y, hydra_pos.x);
        transform.parent.transform.rotation = Quaternion.Euler(new Vector3(0, 0, angle + 90));


    }
}
