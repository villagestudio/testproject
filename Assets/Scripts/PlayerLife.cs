﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlayerLife : MonoBehaviour {

	// Use this for initialization

    public Slider hp;
    public Slider mana;

	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        

        if(Input.GetKey(KeyCode.LeftArrow))
        {
            hp.value -= 1;
        }
        if(Input.GetKey(KeyCode.RightArrow))
        {
            hp.value += 1;
        }

        if (Input.GetKey(KeyCode.DownArrow))
        {
            mana.value -= 1;
        }
        if (Input.GetKey(KeyCode.UpArrow))
        {
            mana.value += 1;
        }
	
	}
}
