﻿using UnityEngine;
using System.Collections;

public class DamageOnHitNoPlat : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Monster")
        {
            EnemyController ec = collision.collider.GetComponent<EnemyController>();
            ec.TakeDamage(5, transform.position, 3, true);
        }

        if (collision.collider.name != "platform")
            Destroy(gameObject);

        
    }
}
