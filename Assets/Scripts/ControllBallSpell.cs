﻿using UnityEngine;
using System.Collections;

public class ControllBallSpell : MonoBehaviour {

	void Start () {
	
	}
    
	void Update () 
    {
        float deadzone = 0.85f;
        Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"));
        if(stickInput.magnitude < deadzone)
        stickInput = Vector2.zero;
        float hax = stickInput.x;
        float vax = stickInput.y;

        float absVelocity=Mathf.Max(Mathf.Abs(rigidbody2D.velocity.x),Mathf.Abs(rigidbody2D.velocity.y));
        if (hax > 0.7f )//&& vax < 0.7f && vax > -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(absVelocity, 0);
            this.transform.rotation = Quaternion.AngleAxis(90, Vector3.forward);
        }
        else if (hax < -0.7f )//&& vax < 0.7f && vax > -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(-absVelocity, 0);
            this.transform.rotation = Quaternion.AngleAxis(270,Vector3.forward);
        }
        else if(vax > 0.7 )//&& hax <0.7f && hax> -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(0, -absVelocity);
            this.transform.rotation = Quaternion.AngleAxis(0,Vector3.forward);
        }
        else if(vax < -0.7 )//&& hax <0.7f && hax> -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(0, absVelocity);
            this.transform.rotation = Quaternion.AngleAxis(-180, Vector3.forward);
        }

	}
}
