﻿using UnityEngine;
using System.Collections;

public class ScreenManager : MonoBehaviour {

    public GameObject menuObject;
	void Start () {

        menuObject.SetActive(false);

	}

    void Update()
    {
        if(Input.GetKeyDown(KeyCode.Escape))
        {
            menuObject.SetActive(!menuObject.activeSelf);
        }
    }

	public void ExitGame()
    {
        Application.Quit();
    }

}
