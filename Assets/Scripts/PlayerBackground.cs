﻿using UnityEngine;
using System.Collections;

public class PlayerBackground : MonoBehaviour {

    public Component layer1;
    public Component layer2;
    public Component layer3;
    public float l1_offset = 0.05f;
    public float l2_offset = 0.01f;
    public float l3_offset = 0.005f;

    float prevX = 0;

	// Use this for initialization
	void Start () {

        prevX = this.transform.position.x;
	}
	
	// Update is called once per frame
	void Update () {
		//here we set the parallax effect
		if (!Variables.playerHitsCameraLeftBound && !Variables.playerHitsCameraRightBound)
		{
			float delta = prevX - transform.position.x;

			Vector2 aux;

			aux = layer1.transform.position;
			aux.x += delta * l1_offset;
			layer1.transform.position = aux;

			aux = layer2.transform.position;
			aux.x += delta * l2_offset;
			layer2.transform.position = aux;

			aux = layer3.transform.position;
			aux.x += delta * l3_offset;
			layer3.transform.position = aux;

			prevX = transform.position.x;
		}
	}
}
