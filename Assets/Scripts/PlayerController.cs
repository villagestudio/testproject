﻿using UnityEngine;
using System.Collections;

public class PlayerController : MonoBehaviour
{

	float currentVelocity;
	float oldVelocity;
	float acceleration;
	float maximumVelocity;
	float jumpVelocity;
	bool pressedJump;
    public static bool WizardMain = false;
    public Sprite N1, N2, W1, W2;
    public GameObject R1, R2;
    public SpellControler controler;

    float clock = 0;
    float clock2 = 0;

    HydraBase.ControllerData oldData1;
    HydraBase.ControllerData oldData2;
	// Use this for initialization
	void Start()
	{
		currentVelocity = 0;
		oldVelocity = 0;
		acceleration = 10;
		maximumVelocity = 15;
		jumpVelocity = 45;
		//canIWalk = true;
		pressedJump = false;
    }

    public void switchInput()
    {
        SpriteRenderer myR1 = R1.GetComponent<SpriteRenderer>();
        SpriteRenderer myR2 = R2.GetComponent<SpriteRenderer>();

        if (WizardMain) 
        {
            WizardMain = false;
            myR1.sprite = N1;
            myR2.sprite = N2;
            acceleration = 30;
        }
        else
        {
            WizardMain = true;
            myR1.sprite = W1;
            myR2.sprite = W2;
            oldData1 = new HydraBase.ControllerData();
            oldData2 = new HydraBase.ControllerData();
            HydraBase.GetNewestData(0,out oldData1);
            HydraBase.GetNewestData(1,out oldData2);
            acceleration = 60;
        }
    }

	// Update is called once per frame
	void Update()
	{
        clock += Time.deltaTime;
        HydraBase.ControllerData data = new HydraBase.ControllerData();
        HydraBase.GetNewestData(1, out data);
        if(immune>0)
        {
            immune -= Time.deltaTime;
        }
        if (Input.GetButton("Check2") && (data.Buttons & HydraBase.HydraButtonState.Start) == 1 && clock > 2)
        {
            switchInput();
            clock = 0;
            return;
        }
        if (!WizardMain)
        {

            if (Variables.playerHitsGround)
            {
                makePlayerMoveHorizontallyOnGround(getGamePadAxis());
                makePlayerJump();
            }
            else
            {
                makePlayerMoveHorizontallyInAir(getGamePadAxis());
                makePlayerPlane();
            }
        }
            
        else
        {
            HydraBase.ControllerData data1 = new HydraBase.ControllerData();
            HydraBase.ControllerData data2 = new HydraBase.ControllerData();
            HydraBase.GetNewestData(0,out data1);
            HydraBase.GetNewestData(1,out data2);



            float dx1 = 0, dx2 = 0;
            if (R1.transform.position.y < -10 && R2.transform.position.y < -10)
            {
                dx1 = oldData1.Position.x - data1.Position.x;
                dx2 = oldData2.Position.x - data2.Position.x;
                if(dx1 < 0)
                {
                    dx2 = -Mathf.Abs(dx2);
                }
                else
                {
                    dx2 = Mathf.Abs(dx2);
                }
            }

                makePlayerMoveHorizontallyOnGround(new Vector2(dx1+dx2, 0));

            oldData1 = data1;
            oldData2 = data2;
            
        }

	}
	
	private void makePlayerMoveHorizontallyInAir(Vector2 stickInput)
	{
		//let the player slow down when he doesnt press any key
		if (stickInput == Vector2.zero)
		{
			Vector3 offset = this.transform.position;
			oldVelocity = currentVelocity;
			if (currentVelocity < 0)
			{
				currentVelocity = Mathf.Min(oldVelocity + (acceleration / 1.5f) * Time.deltaTime, 0);
			} else
			{
				currentVelocity = Mathf.Max(oldVelocity - (acceleration / 1.5f) * Time.deltaTime, 0);
			}
			offset.x += Time.deltaTime * (oldVelocity + currentVelocity) / 2;
			this.transform.position = offset;
		} else
		{
			float newAcceleration = acceleration;
			//if we are moving in a certan direction and suddenly we change the direction
			if ((currentVelocity < 0 && stickInput.x > 0) || (currentVelocity > 0 && stickInput.x < 0))
			{
				newAcceleration = acceleration * 3.0f;
			}
			this.transform.position = getNewPositionForPlayer(newAcceleration, stickInput.x);
		}
	}

	private void makePlayerMoveHorizontallyOnGround(Vector2 stickInput)
	{
		if (stickInput != Vector2.zero)
		{
			//if we are moving in a certan direction and suddenly we change the direction
			if ((currentVelocity < 0 && stickInput.x > 0) || (currentVelocity > 0 && stickInput.x < 0))
			{
				currentVelocity = 0;
				oldVelocity = 0;
			}
			this.transform.position = getNewPositionForPlayer(acceleration, stickInput.x);
		} else
		{
			currentVelocity = 0;
		}
	}
	
	private Vector3 getNewPositionForPlayer(float newAcceleration, float direction)
	{
		oldVelocity = currentVelocity;
		Vector3 location = this.transform.position;
		
		if (direction < 0 && !Variables.playerHitsWorldLeftBound)
		{
			currentVelocity = Mathf.Max(oldVelocity - newAcceleration * Time.deltaTime, -maximumVelocity);
		} else
		if (direction > 0 && !Variables.playerHitsWorldRightBound)
		{
			currentVelocity = Mathf.Min(oldVelocity + newAcceleration * Time.deltaTime, maximumVelocity);
		} else
		{
			currentVelocity = 0;
		}
		location.x += Time.deltaTime * (oldVelocity + currentVelocity) / 2;
		return location;
	}

	private void makePlayerJump()
	{
		if (Input.GetButton("Jump Wizzard") && Variables.playerHitsGround && !pressedJump)
		{
			this.rigidbody2D.velocity = new Vector3(0, jumpVelocity, 0);
		}

		if (Input.GetButton("Jump Wizzard"))
		{
			pressedJump = true;
		} else
		{
			pressedJump = false;
		}
	}

	private void makePlayerPlane()
	{
		if (Input.GetButton("Jump Wizzard"))
		{
			pressedJump = true;
		} else
		{
			pressedJump = false;
		}

		if (Input.GetButton("Jump Wizzard") && this.rigidbody2D.velocity.y < 0 && !Variables.playerHitsGround && pressedJump)
		{
			this.transform.rigidbody2D.gravityScale = 3;
			//this.rigidbody2D.velocity = new Vector3(0, Physics2D.gravity.y / 2.0f * (1), 0);
		} else
		{
			this.transform.rigidbody2D.gravityScale = 7;
		}
	}

	private Vector2 getGamePadAxis()
	{
		float deadzone = 0.85f;
		Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal Wizzard"), Input.GetAxis("Vertical Wizzard"));
		
		if (stickInput.magnitude < deadzone)
		{
			stickInput = Vector2.zero;
		} else
		{
			stickInput = stickInput.normalized * ((stickInput.magnitude - deadzone) / (1 - deadzone));
		}
		
		return stickInput;
	}

	void OnTriggerEnter2D(Collider2D hit)
	{
		if (hit.gameObject.name == "left_camera_bound")
		{
			Variables.playerHitsCameraLeftBound = true;
		} else
		if (hit.gameObject.name == "right_camera_bound")
		{
			Variables.playerHitsCameraRightBound = true;
		}
	}

	void OnTriggerExit2D(Collider2D hit)
	{
		if (hit.gameObject.name == "left_camera_bound")
		{
			Variables.playerHitsCameraLeftBound = false;
		} else
			if (hit.gameObject.name == "right_camera_bound")
		{
			Variables.playerHitsCameraRightBound = false;
		} else
		if (hit.gameObject.name == "left_world_bound")
		{
			Variables.playerHitsWorldLeftBound = false;
		} else
			if (hit.gameObject.name == "right_world_bound")
		{
			Variables.playerHitsWorldRightBound = false;
		}
	}

	void OnCollisionEnter2D(Collision2D hit)
	{
		if (hit.gameObject.name == "left_world_bound")
		{
			Variables.playerHitsWorldLeftBound = true;
		} else
			if (hit.gameObject.name == "right_world_bound")
		{
			Variables.playerHitsWorldRightBound = true;
		} else
			if (hit.gameObject.name == "platform")
		{
			Variables.playerHitsGround = true;
		}
	}

	void OnCollisionExit2D(Collision2D hit)
	{
		if (hit.gameObject.name == "left_world_bound")
		{
			Variables.playerHitsWorldLeftBound = false;
		} else
			if (hit.gameObject.name == "right_world_bound")
		{
			Variables.playerHitsWorldRightBound = false;
		} else
                if (hit.gameObject.name == "platform")
		{
			Variables.playerHitsGround = false;
		}
	}
    float immune;
    internal void TakeDamage(int p,Vector3 poz, float knockBack)
    {
        if (immune <= 0)
        {
            if (GetComponent<AudioSource>() != null)
                GetComponent<AudioSource>().Play();
            GetComponent<PlayerLife>().hp.value -= p;
            float f = this.transform.position.x < poz.x ? -knockBack : knockBack;
            this.transform.position = new Vector3(transform.position.x + f, transform.position.y, transform.position.z);
            GetComponent<Animator>().SetTrigger("attacked");

            if (GetComponent<PlayerLife>().hp.value <= 0)
            {
                WizardMain = false;
                Debug.Log("Died!");
                Application.LoadLevel("Main");
            }
            immune = 2f;
        }
    }
}
