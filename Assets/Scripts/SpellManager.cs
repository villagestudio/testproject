﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public class SpellManager : MonoBehaviour 
{
    public static bool casting = false;
    public GameObject player;
    public GameObject castBar;
    public GameObject elements;
    public GameObject types;


    Dictionary<SpellType,int> spellImages;
    public GameObject[] images;
    Dictionary<SpellType, string> spellNames;
    Dictionary<SpellType, int> spellLimit;


    public GameObject fireBallPrefab;
    public GameObject iceBallPrefab;
    public GameObject lightingBallPrefab;
    public GameObject earthBallPrefab;
    public GameObject minionPrefab;


	void Start () 
    {
        spellImages = new Dictionary<SpellType, int>();
        spellImages.Add(SpellType.Fire, 0);
        spellImages.Add(SpellType.Ice, 1);
        spellImages.Add(SpellType.Lighting, 2);
        spellImages.Add(SpellType.Earth, 3);
        spellImages.Add(SpellType.Control, 4);
        spellImages.Add(SpellType.Buff, 5);
        spellImages.Add(SpellType.Minion, 6);
        spellImages.Add(SpellType.Direction, 7);
        spellTypes = new List<SpellType>();
        numberCasts = new Dictionary<SpellType, int>();

        spellLimit = new Dictionary<SpellType, int>();
        spellLimit.Add(SpellType.Fire, maxSpells);
        spellLimit.Add(SpellType.Ice, maxSpells);
        spellLimit.Add(SpellType.Lighting, maxSpells);
        spellLimit.Add(SpellType.Earth, maxSpells);
        spellLimit.Add(SpellType.Control, 1);
        spellLimit.Add(SpellType.Buff, 1);
        spellLimit.Add(SpellType.Minion, 1);
        spellLimit.Add(SpellType.Direction, 1);

        spellNames = new Dictionary<SpellType, string>();
        spellNames.Add(SpellType.Fire, "FIRE!");
        spellNames.Add(SpellType.Ice, "ICE!");
        spellNames.Add(SpellType.Lighting, "LIGHTING!");
        spellNames.Add(SpellType.Earth, "EARTH!");
        spellNames.Add(SpellType.Control, "CONTROL!");
        spellNames.Add(SpellType.Buff, "BUFF!");
        spellNames.Add(SpellType.Minion, "MUSTACHE!");
        spellNames.Add(SpellType.Direction, "DIRECTION!");

	}
    List<SpellType> spellTypes;
    public int maxSpells;

    Dictionary<SpellType, int> numberCasts;

    int nrFire;
    int nrIce;
    int nrLighting;
    int nrEarth;

    void AddSpell(SpellType type)
    {
        if (!numberCasts.ContainsKey(type))
        {
            numberCasts.Add(type, 1);
            GameObject newspell = Instantiate(images[spellImages[type]]) as GameObject;

            spellTypes.Add(type);

            newspell.transform.SetParent(castBar.transform);



            AudioSource audio = newspell.GetComponent<AudioSource>() as AudioSource;
            if (audio != null) audio.Play();

            newspell.transform.localScale = Vector3.one;
        }
        else if (numberCasts[type] + 1 < spellLimit[type])
        {
            numberCasts[type]++;
            GameObject newspell = Instantiate(images[spellImages[type]]) as GameObject;

            spellTypes.Add(type);

            newspell.transform.SetParent(castBar.transform);



            AudioSource audio = newspell.GetComponent<AudioSource>() as AudioSource;
            if (audio != null) audio.Play();

            newspell.transform.localScale = Vector3.one;
        }
        
    }

	void Update () 
    {
        if (PlayerController.WizardMain)
        {
            if (spellTypes.Count < maxSpells)
            {
                if (elements.activeInHierarchy)
                {
                    if (Input.GetButtonDown("SpellX")) //Lighting
                    {
                        AddSpell(SpellType.Lighting);
                    }
                    if (Input.GetButtonDown("SpellY")) //Ice
                    {
                        AddSpell(SpellType.Ice);
                    }
                    if (Input.GetButtonDown("SpellA")) //Earth
                    {
                        AddSpell(SpellType.Earth);
                    }
                    if (Input.GetButtonDown("SpellB")) //Fire
                    {
                        AddSpell(SpellType.Fire);

                    }
                }
                else if (types.activeInHierarchy)
                {
                    if (Input.GetButtonDown("SpellX")) //Minion
                    {
                        AddSpell(SpellType.Minion);
                    }
                    if (Input.GetButtonDown("SpellY")) //Target
                    {
                        AddSpell(SpellType.Buff);
                    }
                    if (Input.GetButtonDown("SpellA")) //Direction
                    {
                        AddSpell(SpellType.Direction);
                    }
                    if (Input.GetButtonDown("SpellB")) //Control
                    {
                        AddSpell(SpellType.Control);
                    }
                }
            }
            float zaxis = Input.GetAxis("TriggerAxis");
            if (zaxis > 0.7f)
            {
                elements.SetActive(false);
                types.SetActive(true);
            }
            else if (zaxis < -0.7f)
            {

            }
            else
            {
                elements.SetActive(true);
                types.SetActive(false);
            }
            if (Input.GetButtonDown("CastSpell"))
            {
                CastSpell();
            }
        }
        else
        {
            foreach (RectTransform gm in castBar.GetComponentInChildren<RectTransform>())
            {
                Destroy(gm.gameObject);
            }
            spellTypes.Clear();
            this.gameObject.SetActive(false);

            numberCasts.Clear();
        }
	}
    private Vector2 getGamePadAxis2()
    {
        float deadzone = 0.85f;
        Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"));

        if (stickInput.magnitude < deadzone)
        {
            stickInput = Vector2.zero;
        }
        else
        {
            stickInput = stickInput.normalized * ((stickInput.magnitude - deadzone) / (1 - deadzone));
        }

        return stickInput;
    }
    bool SpellElementUnique(SpellType type)
    {
        if (numberCasts.ContainsKey(type) == false) return false;
        switch(type)
        {
            case SpellType.Fire:
                return !(numberCasts.ContainsKey(SpellType.Earth) || numberCasts.ContainsKey(SpellType.Ice) || numberCasts.ContainsKey(SpellType.Lighting));
            case SpellType.Ice:
                return !(numberCasts.ContainsKey(SpellType.Earth) || numberCasts.ContainsKey(SpellType.Fire) || numberCasts.ContainsKey(SpellType.Lighting));
            case SpellType.Lighting:
                return !(numberCasts.ContainsKey(SpellType.Earth) || numberCasts.ContainsKey(SpellType.Ice) || numberCasts.ContainsKey(SpellType.Fire));
            case SpellType.Earth:
                return !(numberCasts.ContainsKey(SpellType.Fire) || numberCasts.ContainsKey(SpellType.Ice) || numberCasts.ContainsKey(SpellType.Lighting));
            default:
                return false;
        }
    }
    void CastSpell()
    {
        
        casting = false;
        foreach (RectTransform gm in castBar.GetComponentInChildren<RectTransform>())
        {
            Destroy(gm.gameObject);
        }
        player.GetComponent<Animator>().SetTrigger("attack");
        Vector2 vtemp = getGamePadAxis2();
        Vector3 directionVector = new Vector3(1,0);
        if (GetNumberOfSpell(SpellType.Fire)>0 && GetNumberOfSpell(SpellType.Direction)>0 && SpellElementUnique(SpellType.Fire))
        {
            player.GetComponent<PlayerLife>().mana.value -= 10;
            GameObject fireball = Instantiate(fireBallPrefab, player.transform.position, Quaternion.AngleAxis(90,Vector3.forward)) as GameObject;
            fireball.rigidbody2D.gravityScale = 0f;

            fireball.transform.localScale += Vector3.one * (GetNumberOfSpell(SpellType.Fire)-1) * 0.25f;
            if (GetNumberOfSpell(SpellType.Control) == 0) fireball.rigidbody2D.AddForce(new Vector2(3500, 0));
            else fireball.rigidbody2D.AddForce(new Vector2(1500, 0));
            if (GetNumberOfSpell(SpellType.Control) > 0)
                fireball.AddComponent<ControllBallSpell>();
            
        }
        if (GetNumberOfSpell(SpellType.Earth) > 0 && GetNumberOfSpell(SpellType.Direction) > 0 && SpellElementUnique(SpellType.Earth))
        {
            player.GetComponent<PlayerLife>().mana.value -= 10;
            GameObject fireball = Instantiate(earthBallPrefab, player.transform.position, Quaternion.AngleAxis(90, Vector3.forward)) as GameObject;
            fireball.rigidbody2D.gravityScale = 0f;
            fireball.transform.localScale += Vector3.one * (GetNumberOfSpell(SpellType.Earth)-1) * 0.25f;
            if (GetNumberOfSpell(SpellType.Control) == 0) fireball.rigidbody2D.AddForce(new Vector2(3500, 0));
            else fireball.rigidbody2D.AddForce(new Vector2(1500, 0));
            if (GetNumberOfSpell(SpellType.Control) > 0)
                fireball.AddComponent<ControllBallSpell>();
        }
        if (GetNumberOfSpell(SpellType.Ice) > 0 && GetNumberOfSpell(SpellType.Direction) > 0 && SpellElementUnique(SpellType.Ice))
        {
            player.GetComponent<PlayerLife>().mana.value -= 10;
            GameObject fireball = Instantiate(iceBallPrefab, player.transform.position, Quaternion.AngleAxis(90, Vector3.forward)) as GameObject;
            fireball.rigidbody2D.gravityScale = 0f;
            fireball.transform.localScale += Vector3.one * (GetNumberOfSpell(SpellType.Ice)-1) * 0.25f;
            if (GetNumberOfSpell(SpellType.Control) == 0) fireball.rigidbody2D.AddForce(new Vector2(3500, 0));
            else fireball.rigidbody2D.AddForce(new Vector2(1500, 0));
            if (GetNumberOfSpell(SpellType.Control) > 0)
                fireball.AddComponent<ControllBallSpell>();
        }
        if (GetNumberOfSpell(SpellType.Lighting) > 0 && GetNumberOfSpell(SpellType.Direction) > 0 && SpellElementUnique(SpellType.Lighting))
        {
            player.GetComponent<PlayerLife>().mana.value -= 10;
            GameObject fireball = Instantiate(lightingBallPrefab, player.transform.position, Quaternion.AngleAxis(90, Vector3.forward)) as GameObject;
            fireball.rigidbody2D.gravityScale = 0f;
            fireball.transform.localScale += Vector3.one * (GetNumberOfSpell(SpellType.Lighting) - 1) * 0.25f;
            if (GetNumberOfSpell(SpellType.Control) == 0) fireball.rigidbody2D.AddForce(new Vector2(3500, 0));
            else fireball.rigidbody2D.AddForce(new Vector2(1500, 0));
            if (GetNumberOfSpell(SpellType.Control) > 0)
                fireball.AddComponent<ControllBallSpell>();
        }
        if (GetNumberOfSpell(SpellType.Minion) > 0 && GetNumberOfSpell(SpellType.Direction) > 0)
        {
            player.GetComponent<PlayerLife>().mana.value -= 10;
            GameObject fireball = Instantiate(minionPrefab, player.transform.position, Quaternion.identity) as GameObject;

            if (GetNumberOfSpell(SpellType.Control) > 0)
                fireball.AddComponent<MustacheMinionCtrl>();
            else
                fireball.AddComponent<MustacheMinionAI>();

            if(SpellElementUnique(SpellType.Fire))
            {
                fireball.AddComponent<ExplodeOnHitNoPlat>();
            }
            else if(SpellElementUnique(SpellType.Ice))
            {
                fireball.AddComponent<StunOnHitNoPlat>();
            }
            else if(SpellElementUnique(SpellType.Lighting))
            {
                fireball.AddComponent<DamageOnHitNoPlat>();
            }
            else if(SpellElementUnique(SpellType.Earth))
            {
                fireball.AddComponent<SlowOnHitNoPlat>();
            }
            else
            {
                fireball.AddComponent<DestroyOnHitNoPlatform>();
            }

        }

        spellTypes.Clear();
        this.gameObject.SetActive(false);

        numberCasts.Clear();
    }
    
    int GetNumberOfSpell(SpellType type)
    {
        if (numberCasts.ContainsKey(type))
        {
            return numberCasts[type];
        }
        else
            return 0;
    }
}
