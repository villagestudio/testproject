﻿using UnityEngine;
using System.Collections;

public class SignScript : MonoBehaviour {

    public GameObject obj;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	}

    void OnTriggerEnter2D(Collider2D x)
    {
        if (x.tag == "Player")
        {
            obj.SetActive(true);
        }
    }
    void OnTriggerExit2D(Collider2D x)
    {
        if (x.tag == "Player")
        {
            obj.SetActive(false);
        }
    }
}
