﻿using UnityEngine;
using System.Collections;

public class MustacheMinionCtrl : MonoBehaviour {

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {

        float deadzone = 0.85f;
        Vector2 stickInput = new Vector2(Input.GetAxis("Horizontal2"), Input.GetAxis("Vertical2"));
        if (stickInput.magnitude < deadzone)
            stickInput = Vector2.zero;
        float hax = stickInput.x;
        float vax = stickInput.y;

        float absVelocity = Mathf.Max(Mathf.Max(Mathf.Abs(rigidbody2D.velocity.x), Mathf.Abs(rigidbody2D.velocity.y)),10f);

        if (hax > 0.7f)//&& vax < 0.7f && vax > -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(absVelocity, rigidbody2D.velocity.y);
        }
        else if (hax < -0.7f)//&& vax < 0.7f && vax > -0.7f)
        {
            this.rigidbody2D.velocity = new Vector2(-absVelocity, rigidbody2D.velocity.y);
        }
	}
}
