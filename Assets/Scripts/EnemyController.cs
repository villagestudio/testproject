﻿using UnityEngine;
using System.Collections;

public class EnemyController : MonoBehaviour
{
	public GameObject player;
	float currentVelocity;
	float oldVelocity;
	float acceleration;
	float maximumVelocity;
	Vector2 initialPosition;
	int direction;
	public float walkingDistance;
	public float followRange;
	public float health;
	public float stun;
	public bool magicResist;
	public bool armor;
	private bool canMoveLeft, canMoveRight;
    float immune = 0f;
	// Use this for initialization
	void Start()
	{
		currentVelocity = 0;
		oldVelocity = 0;
		acceleration = 10;
		maximumVelocity = 10;
		initialPosition = this.transform.position;
		direction = 1;
		//walkingDistance = 0;
		//followRange = 10;
		canMoveLeft = true;
		canMoveRight = true;
		//health = 10;

		//player = GameObject.FindGameObjectWithTag("Player");
	}

	// Update is called once per frame
	void Update()
	{
		if (stun <= 0)
		{
			if (walkingDistance > 0)
			{
				makeEnemyMoveHorizontallyOnGround();
			}
			if (followRange > 0)
			{
				makeEnemyFollowPlayer();
			}
		} else
		{
			stun -= Time.deltaTime;
		}
        if(immune>0)
        {
            immune -= Time.deltaTime;
        }
	}
    bool changedRotationLeft = false;
    bool changedRotationRight = false;
	private void makeEnemyMoveHorizontallyOnGround()
	{
		if (direction < 0 && getNewPositionForEnemy(acceleration, direction).x < initialPosition.x - walkingDistance)
		{
			direction = 1;
			currentVelocity = 0;
            if (!changedRotationLeft)
            {
                //this.transform.rotation = new Quaternion(0, 1, 0, 0);
                this.transform.localScale = new Vector3(-Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);
                changedRotationLeft = true;
                changedRotationRight = false;
            }
		}
		
		if (direction > 0 && getNewPositionForEnemy(acceleration, direction).x > initialPosition.x + walkingDistance)
		{
			direction = -1;
			currentVelocity = 0;
            if (!changedRotationRight)
            {
                this.transform.localScale = new Vector3(Mathf.Abs(this.transform.localScale.x), this.transform.localScale.y, this.transform.localScale.z);
                changedRotationLeft = false;
                changedRotationRight = true;
            }

		}

		this.transform.position = getNewPositionForEnemy(acceleration, direction);
	}

	private void makeEnemyFollowPlayer()
	{
		if (followRange >= Mathf.Abs(player.transform.position.x - this.transform.position.x) &&
		    player.transform.position.y == this.transform.position.y)
		{
			this.transform.position = getNewPositionForEnemy(acceleration, player.transform.position.x - this.transform.position.x);
		} else
		{
			if (this.transform.position.x != initialPosition.x && this.transform.position.y != initialPosition.y)
			{
				float lastDirection = initialPosition.x - this.transform.position.x;
				this.transform.position = getNewPositionForEnemy(acceleration, initialPosition.x - this.transform.position.x);
				float direction2 = initialPosition.x - this.transform.position.x;

				if(Mathf.Sign(direction2) != Mathf.Sign(lastDirection))
				{
                    this.transform.position = initialPosition;
				}
			}
		}
	}

	private bool canEnemyFollowPlayer()
	{
		//if is in range
		if (followRange >= Mathf.Abs(player.transform.position.x - this.transform.position.x))
		{
			if (player.transform.position.y == this.transform.position.y)
			{
				return true;
			}
			//if he is jumping from the same platform as the enemy is on
			if (Mathf.Abs(player.transform.position.y - this.transform.position.y) < 10 && !Variables.playerHitsGround)
			{
				return true;
			}
		}
		return false;
	}

	private Vector3 getNewPositionForEnemy(float newAcceleration, float direction)
	{
		oldVelocity = currentVelocity;
		Vector3 location = this.transform.position;
		
		if (direction < 0 && canMoveLeft)
		{
			currentVelocity = -maximumVelocity;//Mathf.Max(oldVelocity - newAcceleration * Time.deltaTime, -maximumVelocity);
		} else
		if (direction > 0 && canMoveRight)
		{
			currentVelocity = maximumVelocity;//Mathf.Min(oldVelocity + newAcceleration * Time.deltaTime, maximumVelocity);
		} else
		{
			currentVelocity = 0;
		}
		location.x += Time.deltaTime * (oldVelocity + currentVelocity) / 2;
		return location;
	}

	private void onHit()
	{
		health -= 4;
		if (health < 0)
		{
		}
	}

	void OnTriggerEnter2D(Collider2D hit)
	{
		if (hit.gameObject.name == "left_camera_bound")
		{
			//Variables.playerHitsCameraLeftBound = true;
		}
	}

	void OnTriggerExit2D(Collider2D hit)
	{
		if (hit.gameObject.name == "left_camera_bound")
		{
			//Variables.playerHitsCameraLeftBound = false;
		}
	}

	void OnCollisionEnter2D(Collision2D hit)
	{
		if (hit.gameObject.name == "left_world_bound")
		{
			canMoveLeft = false;
			//Variables.playerHitsWorldLeftBound = true;
		} else
			if (hit.gameObject.name == "right_world_bound")
		{
			canMoveRight = false;
			//Variables.playerHitsWorldLeftBound = true;
		}
        if(hit.gameObject.tag=="Player")
        {
            player.GetComponent<PlayerController>().TakeDamage(5,this.transform.position,6);
            GetComponent<Animator>().SetTrigger("attack");
        }
	}

	void OnCollisionExit2D(Collision2D hit)
	{
		if (hit.gameObject.name == "left_world_bound")
		{
			canMoveLeft = true;
			//Variables.playerHitsWorldLeftBound = false;
		} else
			if (hit.gameObject.name == "right_world_bound")
		{
			canMoveRight = true;
			//Variables.playerHitsWorldLeftBound = true;
		}
	}

    internal void TakeDamage(int p, Vector3 poz, float knockBack, bool magicDamage)
    {
		if ( !(magicDamage && magicResist) && immune<=0)
		{
            if(GetComponent<AudioSource>()!=null)
                GetComponent<AudioSource>().Play();
			health -= p;
			float f = this.transform.position.x < poz.x ? -knockBack : knockBack;
			Animator anim = GetComponent<Animator>();
			anim.SetTrigger("attacked");
			this.transform.position = new Vector3(transform.position.x + f, transform.position.y, transform.position.z);
			if (health <= 0)
			{
				Destroy(gameObject);
			}
            immune = 1.5f;
		}
    }
}
