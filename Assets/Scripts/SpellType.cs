﻿using System.Collections;

public enum SpellType
{
    Fire=0,
    Ice=1,
    Lighting=2,
    Earth=3,
    Control=4,
    Buff=5,
    Minion=6,
    Direction=7,
}
