﻿using UnityEngine;
using System.Collections;

public class Clouds : MonoBehaviour {

    public float speed = 1;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
        Vector2 pos = this.transform.position;
        pos.x += speed * Time.deltaTime;
        this.transform.position = pos;
	}
}
