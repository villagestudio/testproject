﻿using UnityEngine;
using System.Collections;

public class Camera : MonoBehaviour
{

	public GameObject cameraTarget; // object to look at or follow
	public GameObject mainGround; // object to look at or follow
	public GameObject player; // player object for moving
	public GameObject leftBound; // object where the camera stops following the player
	public GameObject rightBound; // object where the camera stops following the player
	
	public float smoothTime = 0.1f; // time for dampen
	public bool cameraFollowX = true; // camera follows on horizontal
	public bool cameraFollowY = true; // camera follows on vertical
	public bool cameraFollowHeight = true; // camera follow CameraTarget object height
	public float cameraHeight = 2.5f; // height of camera adjustable
	public Vector2 velocity; // speed of camera movement
	
	private Transform thisTransform; // camera Transform
	
	// Use this for initialization
	void Start()
	{
		thisTransform = transform;
	}
	
	// Update is called once per frame
	void Update()
	{
		float tempX = 0;
		//if we dont collide with the left and right camera bounds
		if (!Variables.playerHitsCameraLeftBound && !Variables.playerHitsCameraRightBound)
		{
			tempX = Mathf.SmoothDamp(thisTransform.position.x, cameraTarget.transform.position.x, ref velocity.x, smoothTime);
		} else
			if (Variables.playerHitsCameraLeftBound)
		{
			tempX = Mathf.SmoothDamp(thisTransform.position.x, leftBound.transform.position.x, ref velocity.x, smoothTime);
		}
		else
			if (Variables.playerHitsCameraRightBound)
		{
			tempX = Mathf.SmoothDamp(thisTransform.position.x, rightBound.transform.position.x, ref velocity.x, smoothTime);
		}

		if (cameraFollowX)
		{
			thisTransform.position = new Vector3(tempX, thisTransform.position.y, thisTransform.position.z);
			//thisTransform.position.x = Mathf.SmoothDamp (thisTransform.position.x, cameraTarget.transform.position.x, ref velocity.x, smoothTime); // Here i get the error
		}
		if (cameraFollowY)
		{
			// to do    
		}
		if (!cameraFollowX & cameraFollowHeight)
		{
			// to do
		}
	}
}
