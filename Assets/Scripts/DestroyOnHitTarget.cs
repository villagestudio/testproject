﻿using UnityEngine;
using System.Collections;

public class DestroyOnHitTarget : MonoBehaviour {

	// Use this for initialization
	void Start () {

        Destroy(gameObject, 6f);

	}
	
	void Update () {
	
	}

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.collider.tag == "Monster")
        {
            EnemyController ec = collision.collider.GetComponent<EnemyController>();
            ec.TakeDamage(3, transform.position, 3, true);
        }
        if (collision.collider.tag != "Player")
        Destroy(gameObject);
    }

}
